﻿using Microsoft.EntityFrameworkCore;
using Otus.Teaching.PromoCodeFactory.Core.Domain.Administration;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using System;
using System.Collections.Generic;
using System.Numerics;
using System.Text;

namespace Otus.Teaching.PromoCodeFactory.DataAccess.Data
{
    public class DataContext : DbContext
    {
        public DbSet<Customer> Customers { get; set; }
        public DbSet<Preference> Preferences { get; set; }
        public DbSet<Role> Roles { get; set; }
        public DbSet<Employee> Employees { get; set; }
        public DataContext()
        {

        }
        public DataContext(DbContextOptions<DataContext> options) : base(options)
        {
            Database.EnsureDeleted();
            Database.EnsureCreated();
            this.Customers.AddRange(FakeDataFactory.Customers);
            this.SaveChanges();
            this.Preferences.AddRange(FakeDataFactory.Preferences);
            this.SaveChanges();
            this.Roles.AddRange(FakeDataFactory.Roles);
            this.SaveChanges();
            foreach (Employee employee in FakeDataFactory.Employees)
            {
                var role = Roles.Find(employee.Role.Id);
                if (role != null) 
                {
                    var newEmployee = new Employee()
                    { 
                        Id = Guid.NewGuid(),
                        FirstName = employee.FirstName,
                        LastName = employee.LastName,   
                        Email = employee.Email,
                        AppliedPromocodesCount= employee.AppliedPromocodesCount,    
                        Role = role
                    };
                    this.Employees.Add(newEmployee);
                }
            }
            this.SaveChanges();
        }
        protected override void OnConfiguring(DbContextOptionsBuilder options)
        => options.UseSqlServer("server=localhost\\sqlexpress;database=PromoCodeFactorydb;TrustServerCertificate=True;trusted_connection=true");
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Customer>()
            .HasMany(p => p.Preferences)
            .WithMany(c => c.Customers);
        }
    }
}
